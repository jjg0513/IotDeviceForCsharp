# IotDeviceToolHepler

#### 介绍
c# 香橙派引脚开发，WiringOP for dotnet ,希望一起贡献，项目一起成长

支持国产香橙派 ，简单易用


#### 软件架构
软件架构说明

c#   .net6，引用WiringOP c++类库进行 香橙派引脚 gpio,i2c通讯,Serial 串口等非常多方面的引用进行开发
 
|  序号 |已支持传感器   |备注   |
|---|---|---|
|  1 | 峰鸣器  |  DeviceBuzzer|
|  2 | 串口\rs485  | DeviceSerial  |
|  3 | dht11温湿传感器  |  SensorDHT11|
|  4 | l298n高压电机驱动板 | DeviceStepperL298N|
|  5 | 香橙派主板温度  | OrangePIMachine|
|  6 | sht30温湿传感器  | Device_Sensor_SHT30TempAndHumService  |
|  7 | uln2003步进电机驱动板  | SensorSHT30|
|  8 | 工业级光强度B-RS-L30传感器rs485 | SensorLightBRSL30|
|  9 | 土壤湿度传感器  |  SensorSoilHumidityAo |
|  10 | 空气质量传感器  | 期待您参与开发  |
|  11 | led灯  | DeviceLed3Color|
|  12 | 显示屏  |  DeviceDisplay |
|  12 | 继电器|gpio高低电平控制  |  OrangePiGpio|
|  13 | 光强度传感器LM393 | SensorLightLM393Ao|
|  14 | orangepi i96主板 gpio使用 | OrangeI96Gpio|

#### 安装教程

1.  香橙派安装好WiringOP，[安装教程在我的csdn](https://blog.csdn.net/qq_16005627/article/details/126777995)
2.  安装好WiringOP后，确认香橙派目录 /usr/lib目录生成了libwiringPi.so库
3.  安装好.net6运行环境
4.  部署本项目程序到香橙派运行即可
5. 项目可以通过下载Nuget包使用：IotDeviceToolHepler 使用例子见：https://blog.csdn.net/qq_16005627/article/details/126817568
#### 使用说明

1.  本人香橙派使用ubuntn系统
2.  见使用说明文章：https://blog.csdn.net/qq_16005627/article/details/126817568?csdn_share_tail=%7B%22type%22%3A%22blog%22%2C%22rType%22%3A%22article%22%2C%22rId%22%3A%22126817568%22%2C%22source%22%3A%22qq_16005627%22%7D
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_dev 分支
3.  提交代码
4.  新建 Pull Request
5.  期待你的参与，若参与贡献，微x+:linyihong

#### 运行

增加nuget包，项目可以通过Nuget包引用：IotDeviceToolHepler

或通过下载本源码库使用
demo项目Program.cs
```
 class Program
    {
        static void Main(string[] args)
        {
             //gpio操作
            DemoPinMode.Run();
            //serial 串口，使用wiringop类库
            //DemoSerial.Run();
            //serial DemoByIOPort  串口 使用SerialPort类库 
            //DemoByIOPort.Run();
            //土壤湿度传感器
            SensorSoilHumidityAo.test();
            //sht30环境温湿度传感器
            SensorSHT30.test();
            //485光感传感器
            SensorLightBRSL30.test();
            //gipo 高低电平操作
            OrangePiGpio.test();

        }
    }
```
关于orangepi  i96板子的使用说明见：
https://blog.csdn.net/qq_16005627/article/details/127015266

加微信 进大群

![输入图片说明](image.png)