﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.DeviceForRaspberryPi
{

    /// <summary>
    /// //步进电机28byj48+uln2003驱动板4相5线
    /// </summary>
    public static class Device_StepperULN2003
    {
        static int pin_19 = 19; //接In1
        static int pin_26 = 26;//接In2
        static int pin_16 = 16;//接In3
        static int pin_20 = 20;//接In4

        /// <summary>
        /// 调用示例
        /// </summary>
        public static void test()
        {
            while (true)
            {
                Console.WriteLine("输入b逆时针转，输入f顺时针转");
                //控制台中输入内容
                string key = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(key))
                {
                    tun(key);
                }
            }
        }

        public static void tun(string runType)
        {

            GpioController gpioController = new GpioController();
            gpioController.OpenPin(pin_19, PinMode.Output);
            gpioController.OpenPin(pin_26, PinMode.Output);
            gpioController.OpenPin(pin_16, PinMode.Output);
            gpioController.OpenPin(pin_20, PinMode.Output);
            if (runType == "b")
            {   //# 逆时针
                setBack(gpioController);
            }
            else if (runType == "f")
            {
                //# 顺时针
                setForward(gpioController);
            }
            gpioController.ClosePin(pin_19);
            gpioController.ClosePin(pin_26);
            gpioController.ClosePin(pin_16);
            gpioController.ClosePin(pin_20);
        }
        public static void setValue(GpioController GPIO, int w1, int w2, int w3, int w4)
        {
            GPIO.Write(pin_19, w1);
            GPIO.Write(pin_26, w2);
            GPIO.Write(pin_16, w3);
            GPIO.Write(pin_20, w4);

        }
        //# 逆时针
        public static void setBack(GpioController GPIO)
        {
            Console.WriteLine("开始逆时针转");
            //256半圈
            for (int i = 0; i < 256; i++)
            {
                int delay = 3;//毫秒
                setValue(GPIO, 0, 0, 0, 1);
                Thread.Sleep(delay);
                setValue(GPIO, 0, 0, 1, 0);
                Thread.Sleep(delay);
                setValue(GPIO, 0, 1, 0, 0);
                Thread.Sleep(delay);
                setValue(GPIO, 1, 0, 0, 0);
                Thread.Sleep(delay);
            }
            setValue(GPIO, 0, 0, 0, 0);//电机断电，不然会发热
        }
        //#顺时针
        public static void setForward(GpioController GPIO)
        {
            Console.WriteLine("开始顺时针转");
            //256半圈
            for (int i = 0; i < 256; i++)
            {
                int delay = 2;//毫秒
                setValue(GPIO, 1, 0, 0, 0);
                Thread.Sleep(delay);
                setValue(GPIO, 0, 1, 0, 0);
                Thread.Sleep(delay);
                setValue(GPIO, 0, 0, 1, 0);
                Thread.Sleep(delay);
                setValue(GPIO, 0, 0, 0, 1);
                Thread.Sleep(delay);
            }
            setValue(GPIO, 0, 0, 0, 0);//电机断电，不然会发热
        }
    }
}
