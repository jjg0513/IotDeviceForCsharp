﻿
using Iot.Device.Sht3x;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.I2c;
using System.Text;
using System.Threading;
using System.Linq;
using IotDeviceToolHepler.Model.Device;

namespace IotDeviceToolHepler.DeviceForRaspberryPi
{
    public static class Sensor_LightLM393Ao
    {

        //运行调用例子
        public static void test()
        {
            while (true)
            {
                ModelLightLM393AoInfo lightLM393AoInfo = getData();
                Thread.Sleep(10000);
            }
        }
        public static ModelLightLM393AoInfo getData()
        {
            ModelLightLM393AoInfo lightLM393AoInfo = new ModelLightLM393AoInfo();
            int pinNum = 17;
            GpioController gpioController = new GpioController();
            gpioController.OpenPin(pinNum, PinMode.Input);
            PinValue pinValue = gpioController.Read(pinNum);
            if (pinValue == PinValue.High)
            {
                lightLM393AoInfo.lightState = 1;
                Console.WriteLine(" 天黑了 ");
            }
            else if (pinValue == PinValue.Low)
            {
                lightLM393AoInfo.lightState = 0;
                Console.WriteLine(" 天亮了 ");
            }
            byte i2cAddrressByte = (byte)Convert.ToInt32("0x48", 16);//16进制转10进制
            //AO模拟转数字光照强度值
            I2cConnectionSettings set = new(1, i2cAddrressByte);
            I2cDevice dev = I2cDevice.Create(set);
            dev.WriteByte(i2cAddrressByte);
            lightLM393AoInfo.lightAo = dev.ReadByte();
            Console.WriteLine("光线强度：" + lightLM393AoInfo.lightAo);
            return lightLM393AoInfo; ;
        }
    }
}


