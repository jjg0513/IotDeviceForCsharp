﻿using Iot.Device.Gpio.Drivers;
using IotDeviceToolHepler.Model.Constant;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Utils
{
    /// <summary>
    /// 香橙子调用WiringOP操作gpio Output 高低电平
    /// </summary>
    public class UtilsOrangePiGpio
    {
        //运行调用例子
        public void test()
        {

            Console.WriteLine("输入引脚WPI编号");
            //控制台中输入内容
            int pinNum = int.Parse(Console.ReadLine());
            Console.WriteLine("输入o打开，输入c关闭");
            string command = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(command))
            {
                setGpioOutputMode(command, pinNum);
            }

        }
        /// <summary>
        /// pin输入高电平或低电平
        /// </summary>
        /// <param name="command">open/close</param>
        /// <param name="pinNum">wpi编号</param>
        /// <returns>状态</returns>
        public string setGpioOutputMode(string command, int pinNum)
        {

            try
            {
                Setup.WiringPiPiSetup();
                GPIO.PinMode(pinNum, WiringPi.Output);

                if (command == "open")
                {   //open

                    GPIO.DigitalWrite(pinNum, WiringPi.High);
                    //Console.WriteLine("执行开");
                    return ConstantGpio.GpioPinOpen;
                }
                else if (command == "close")
                {   //close

                    GPIO.DigitalWrite(pinNum, WiringPi.Low);
                    // Console.WriteLine("执行关");
                    return ConstantGpio.GpioPinClose;
                }

                return ConstantGpio.GpioPinError;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return ConstantGpio.GpioPinError;
            }
        }
        /// <summary>
        /// 获取引脚输出状态：高电平或低电平状态
        /// </summary>
        /// <param name="pinNum">wpi引脚编号</param>
        /// <returns>返回状态</returns>
        public string getGpioOutputMode(int pinNum)
        {
            try
            {
                Setup.WiringPiPiSetup();
                GPIO.PinMode(pinNum, WiringPi.Output);
                int value = GPIO.DigitalRead(pinNum);
                if (value == WiringPi.High)
                {
                    //Console.WriteLine("获得状态 开");
                    return ConstantGpio.GpioPinOpen;
                }
                else if (value == WiringPi.Low)
                {

                    // Console.WriteLine("获得状态  关");
                    return ConstantGpio.GpioPinClose;
                }
                return ConstantGpio.GpioPinError;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return ConstantGpio.GpioPinError;
            }
        }
        /// <summary>
        /// 获取引脚输入状态：高电平或低电平状态
        /// </summary>
        /// <param name="pinNum">wpi引脚编号</param>
        /// <returns>返回状态</returns>
        public string getGpioInputMode(int pinNum)
        {
            try
            {
                Setup.WiringPiPiSetup();
                GPIO.PinMode(pinNum, WiringPi.Input);
                int value = GPIO.DigitalRead(pinNum);
                if (value == WiringPi.High)
                {
                    //Console.WriteLine("获得状态 开");
                    return ConstantGpio.GpioPinOpen;
                }
                else if (value == WiringPi.Low)
                {

                    // Console.WriteLine("获得状态  关");
                    return ConstantGpio.GpioPinClose;
                }
                return ConstantGpio.GpioPinError;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return ConstantGpio.GpioPinError;
            }
        }

        public void SetGpioModel(int pin, PinValue pinValue)
        {
            GpioController gpioController = new GpioController(PinNumberingScheme.Logical, new OrangePiZero2Driver());
            gpioController.OpenPin(pin, PinMode.Output);
            gpioController.Write(pin, pinValue);
            gpioController.ClosePin(pin);
            //Thread.Sleep(3000);
            //gpioController.Write(pin, PinValue.Low);
            //Thread.Sleep(3000);
        }
    }
}
