﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using System.Device.Gpio;

namespace IotDeviceToolHepler.Utils
{
    /// <summary>
    ///注意：这里仅限树莓派，用高低点
    /// </summary>
    public class UtilsRaspberryPiGpio
    { //运行调用例子
        public void test()
        {

            Console.WriteLine("输入引脚WPI编号");
            //控制台中输入内容
            int pinNum = int.Parse(Console.ReadLine());
            Console.WriteLine("输入o打开，输入c关闭");
            string command = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(command))
            {
                jiDianQiState(command, pinNum);
            }

        }

        public void jiDianQiState(string command, int pinNum)
        {
            GpioController gpioController = new GpioController();
            gpioController.OpenPin(pinNum, PinMode.Output);
            if (command == "open")
            {
                gpioController.Write(pinNum, PinValue.High);
            }
            else
            {
                gpioController.Write(pinNum, PinValue.Low);
            }
            PinValue pinValue = gpioController.Read(pinNum);
            if (pinValue == PinValue.High)
            {
                Console.WriteLine("执行开");
            }
            else if (pinValue == PinValue.Low)
            {
                Console.WriteLine("执行关");
            }

        }
    }
}
