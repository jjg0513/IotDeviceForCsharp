﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.Pwm;
using System.Device.Pwm.Drivers;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace IotDeviceToolHepler.Utils
{
    /// <summary>
    ///获取香橙派机子的相关信息
    /// </summary>
    public class UtilsRaspberryPiMachine
    {
        //运行调用例子
        public void test()
        {
            Console.WriteLine("设备id:" + getRaspberryPiDeviceId());
        }
        /// <summary>
        /// 获取树梅派设备id
        /// </summary>
        /// <returns></returns>
        public string getRaspberryPiDeviceId()
        {
            string deviceId = "";
            //树梅派的方法 Raspberry
            string[] tmp = File.ReadAllLines("/proc/cpuinfo", Encoding.UTF8);
            for (int i = 0; i < tmp.Length; i++)
            {
                if (tmp[i].StartsWith("Serial"))
                {
                    Console.WriteLine("设备码：" + tmp[i].Substring(tmp[i].IndexOf(':') + 2));
                    deviceId = tmp[i].Substring(tmp[i].IndexOf(':') + 2);

                }
            }
            return deviceId;
        }
    }
}
