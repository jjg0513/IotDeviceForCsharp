﻿using Iot.Device.Buzzer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace IotDeviceToolHepler.Utils
{
    /// <summary>
    /// 由于i96比较特别，不能用wiringOP, 官方推荐以下方法，通过opoi进行i96引脚操作，,
    /// 类库为于项目：opio_file下，复制到你的/usr/dotnet/自建目录下面即可 或在线上下载opoi, https://wiki.pbeirne.com/patb/opio 
    /// https://discuss.96boards.org/t/resources-for-the-i96-orangepi/11444
    /// </summary>
    public class UtilsOrangeI96Gpio
    {
        //运行调用例子
        public static void test()
        {
            while (true)
            {
                Console.WriteLine("输入o打开，输入c关闭");
                //控制台中输入内容
                string command = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(command))
                {
                    setGpioModeOutPut(28, "on");

                }
            }
        }
        /// <summary>
        /// opio存放目录
        /// </summary>
        static string opioPath = "/usr/dotnet/opio/";
        /// <summary>
        /// pin输入高电平或低电平
        /// </summary>
        /// <param name="pin"></param>
        /// <param name="state"></param>
        public static void setGpioModeOutPut(int pin, string state)
        {

            string command = opioPath + "opio  mode " + pin + " out";
            processCommond(command);
            if (state == "on")
            {
                command = opioPath + "opio  write " + pin + " on";
                processCommond(command);
            }
            else if (state == "off")
            {
                command = opioPath + "opio  write " + pin + " off";
                processCommond(command);
            }
        }


        /// <summary>
        /// pin输入高电平或低电平
        /// </summary>
        /// <param name="pin"></param>
        /// <param name="state"></param>
        public static void setGpioModeInPut(int pin, string state)
        {
            string command = opioPath + "opio  mode " + pin + " in";
            processCommond(command);
            if (state == "on")
            {
                command = opioPath + "opio  write " + pin + " on";
                processCommond(command);
            }
            else if (state == "off")
            {
                command = opioPath + "opio  write " + pin + " off";
                processCommond(command);
            }
        }
        /// <summary>
        /// 读取pin值  返回 1 高电平，0低电平
        /// </summary>
        /// <param name="pin"></param>
        /// <returns></returns>
        public static string getGpioRead(int pin)
        {

            string command = opioPath + "opio  read " + pin + "";
            return processCommond(command);

        }
        //执行
        public static string processCommond(string command)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo("/bin/bash", "")
            };
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            process.Start();
            process.StandardInput.WriteLine(command);

            process.StandardInput.Close();

            var cpuInfo = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            process.Dispose();
            var lines = cpuInfo.Split('\n');
            foreach (var item in lines)
            {
                Console.WriteLine(item);
            }
            return cpuInfo;
        }

    }
}
