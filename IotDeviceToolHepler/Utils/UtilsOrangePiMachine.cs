﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.Pwm;
using System.Device.Pwm.Drivers;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace IotDeviceToolHepler.Utils
{
    /// <summary>
    ///获取香橙派机子的相关信息
    /// </summary>
    public class UtilsOrangePiMachine
    {
        //运行调用例子
        public void test()
        {
            Console.WriteLine("设备id:" + getOrangePiDeviceId());
            while (true)
            {
                Console.WriteLine($@"CPU温度:{getCpuTemperature()}°");
                Console.WriteLine($@"GPU温度:{getGpuTemperature()}°");
                Console.WriteLine($@"总共内存:{getMemoryUseStatus(0)} MB");
                Console.WriteLine($@"已经使用:{getMemoryUseStatus(1)} MB");
                Console.WriteLine($@"还未使用:{getMemoryUseStatus(2)} MB");
                Thread.Sleep(3000);
            }


        }
        /// <summary>
        /// 获取CPU温度
        /// </summary>
        /// <returns></returns>
        public float getCpuTemperature()
        {
            return float.Parse(File.ReadAllText(@"/sys/class/thermal/thermal_zone0/temp")) / 1000;
        }
        /// <summary>
        /// 获取GPU温度
        /// </summary>
        /// <returns></returns>
        public float getGpuTemperature()
        {
            return float.Parse(File.ReadAllText(@"/sys/class/thermal/thermal_zone1/temp")) / 1000;
        }
        /// <summary>
        /// 获取内存使用情况
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public int getMemoryUseStatus(int info)
        {
            string meminfo = File.ReadAllText(@"/proc/meminfo");
            //给出一段参考，实际情况可以删掉
            /*string meminfo =
            @"MemTotal:         765604 kB
            MemFree:          329032 kB
            MemAvailable:     498452 kB
            Buffers:           23168 kB
            Cached:           200980 kB
            SwapCached:            0 kB
            Active:           249832 kB
            Inactive:         112576 kB
            Active(anon):     138644 kB
            Inactive(anon):    17800 kB
            Active(file):     111188 kB
            Inactive(file):    94776 kB
            Unevictable:          16 kB
            Mlocked:              16 kB
            SwapTotal:        102396 kB
            SwapFree:         102396 kB
            Dirty:               132 kB
            Writeback:             0 kB
            AnonPages:        138280 kB
            Mapped:           109408 kB
            Shmem:             18180 kB
            Slab:              49476 kB
            SReclaimable:      16864 kB
            SUnreclaim:        32612 kB
            KernelStack:        2200 kB
            PageTables:         6312 kB
            NFS_Unstable:          0 kB
            Bounce:                0 kB
            WritebackTmp:          0 kB
            CommitLimit:      485196 kB
            Committed_AS:    1356796 kB
            VmallocTotal:    1294336 kB
            VmallocUsed:           0 kB
            VmallocChunk:          0 kB
            Percpu:              656 kB
            CmaTotal:           8192 kB
            CmaFree:            6028 kB";*/
            var memory = meminfo.Split(' ').Where(o => o != string.Empty).ToList();
            return int.Parse(memory[info * 2 + 1]) / 1024;//返回MB
        }


        /// <summary>
        /// 获取香橙派设备id
        /// </summary>
        /// <returns></returns>
        public string getOrangePiDeviceId()
        {
            string deviceId = "";
            // 香橙派系统ubuntu 的方法
            string[] tmp = File.ReadAllLines("/sys/class/sunxi_info/sys_info", Encoding.UTF8);
            Console.WriteLine(tmp.Length);
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                if (tmp[i].StartsWith("sunxi_chipid"))
                {
                    Console.WriteLine("设备码：" + tmp[i].Substring(tmp[i].IndexOf(':') + 2));
                    deviceId = tmp[i].Substring(tmp[i].IndexOf(':') + 2);
                }
            }
            return deviceId;
        }
    }
}
