﻿using Iot.Device.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Utils
{
    public class UtilsByte
    {
        /// <summary>
        /// 补码形式= 反码低位加1
        /// </summary>
        /// <param name="resultText"></param>
        /// <returns></returns>
        public static string GetBuMaBinary(string Text)
        {   
              //负数的反码为对该数的原码除符号位外各位取反
                string sAnticode = "";
                for (int i =0; i < Text.Length-1; i++)
                {
                    sAnticode += (Text[i] == '0' ? "1" : "0");
                }
            sAnticode = sAnticode + "1";
            return sAnticode;
        }

   
    }
}
