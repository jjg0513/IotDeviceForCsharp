﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Utils
{
    public static class UtilsSystemInfoHelper
    {

        /// <summary>
        /// 获取本地IP
        /// </summary>
        /// <returns></returns>

        public static string GetLocalIP()
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                    .Select(p => p.GetIPProperties())
                    .SelectMany(p => p.UnicastAddresses)
                    .FirstOrDefault(p => p.Address.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(p.Address))?.Address.ToString();
        }

        /// <summary>
        /// 取系统名
        /// </summary>
        /// <returns></returns>
        public static string GetSystemNameAndVersion()
        {
            //bool isWindows = System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
            //使用OSDescription获取操作系统版本信息，如可能值为Microsoft Windows 10.0.10586
            return RuntimeInformation.OSDescription;

        }
        /// <summary>
        /// 本地MAC
        /// </summary>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static List<string> GetActiveMacAddress(string separator = "-")
        {
            try
            {
                //本地计算机网络连接信息
                IPGlobalProperties computerProperties = IPGlobalProperties.GetIPGlobalProperties();
                //获取本机电脑名
                var HostName = computerProperties.HostName;
                //获取域名
                var DomainName = computerProperties.DomainName;
                //获取本机所有网络连接
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

                var macAddress = new List<string>();
                if (nics == null || nics.Length < 1)
                {
                    //Debug.WriteLine("  No network interfaces found.");
                    return macAddress;
                }

                //Debug.WriteLine("  Number of interfaces .................... : {0}", nics.Length);
                foreach (NetworkInterface adapter in nics.Where(c =>
                    c.NetworkInterfaceType != NetworkInterfaceType.Loopback && c.OperationalStatus == OperationalStatus.Up))
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();

                    var unicastAddresses = properties.UnicastAddresses;
                    if (unicastAddresses.Any(temp => temp.Address.AddressFamily == AddressFamily.InterNetwork))
                    {
                        var address = adapter.GetPhysicalAddress();

                        if (string.IsNullOrEmpty(separator))
                        {
                            macAddress.Add(address.ToString());
                        }
                        else
                        {
                            var MACIp = "";
                            byte[] bytes = address.GetAddressBytes();
                            for (int i = 0; i < bytes.Length; i++)
                            {
                                MACIp += bytes[i].ToString("X2");

                                if (i != bytes.Length - 1)
                                {
                                    MACIp += separator;
                                }
                            }

                            macAddress.Add(MACIp);
                        }
                    }
                }
                return macAddress;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}

