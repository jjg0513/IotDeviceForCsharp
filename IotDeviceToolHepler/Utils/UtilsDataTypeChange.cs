﻿using SixLabors.ImageSharp.PixelFormats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace IotDeviceToolHepler.Utils
{
    public class UtilsDataTypeChange
    {
        #region 
        /// <summary>
        /// 十六进制字符串转十进制
        /// </summary>
        /// <param name="str">十六进制字符</param>
        /// <returns></returns>
        public int hex16Convert10(string str)
        {
            int res = 0;

            try
            {
                str = str.Trim().Replace(" ", "");//移除空字符
                //方法1
                res = int.Parse(str, System.Globalization.NumberStyles.AllowHexSpecifier);
                //方法2
                //int r2 = System.Int32.Parse(str, System.Globalization.NumberStyles.HexNumber);
                //Console.WriteLine(r2);
                //方法3
                //int r3 = Convert.ToInt32(str, 16);
                //Console.WriteLine(r3);

            }
            catch (Exception e)
            {
                res = 0;
            }

            return res;

        }
        #endregion
        /// <summary>
        /// 字符串转16位字节
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public byte[] strToHexByte(string hexString)

        {

            hexString = hexString.Replace(" ", "");

            if (hexString.Length % 2 != 0)

                hexString += " ";

            byte[] returnBytes = new byte[hexString.Length / 2];

            for (int i = 0; i < returnBytes.Length; i++)

                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);

            return returnBytes;

        }
        #region 字节型转十六进制字符串

        /// <summary>

        /// 字节数组转16进制字符串

        /// </summary>

        /// <param name="bytes"></param>

        /// <returns></returns>

        public static string byteToHexStr(List<byte> bytes)

        {

            string returnStr = "";

            if (bytes != null)

            {

                for (int i = 0; i < bytes.Count; i++)

                {

                    returnStr += bytes[i].ToString("X2");

                }

            }

            return returnStr;

        }
        /// <summary>
        /// 16位字节转字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public string byteToHexStr(byte[] bytes)

        {
          
            string returnStr = "";

            if (bytes != null)
            {

                for (int i = 0; i < bytes.Length; i++)

                {
                    if (returnStr != "")
                    {
                        returnStr = returnStr + " ";
                    }
                    returnStr += bytes[i].ToString("X2");

                }

            }
            return returnStr;

        }
        #endregion

        /// <summary>
        /// 字节数组转16进制字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string byteToHexStr(byte[] bytes, int byteslength)
        {
         
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < byteslength; i++)
                {
                    returnStr += bytes[i].ToString("X2") + " ";
                }
            }
            return returnStr;
        }

        /// <summary>
        /// 二进制转换为十进制  把二进制数据"11011111"转换为十进制
        /// </summary>
        /// <param name="testStr"></param>
        /// <returns></returns>
        public static int binaryToDecimalStr(string testStr)
        {
            return Convert.ToInt32(testStr, 2);
        }
        /// <summary>
        /// 十进制转换为二进制 把十进制整数220转换为二进制：
        /// </summary>
        /// <param name="testValue"></param>
        /// <returns></returns>
        public static string decimaToBinaryTolStr(int testValue)
        {
            return Convert.ToString(testValue, 2);
        }
        /// <summary>
        /// 十六进制转二进制 
        /// </summary>
        /// <param name="hexString">FF9B</param>
        /// <returns></returns>
        public static string hexToBinaryTolStr(string hexString)
        {
            try
            {
                string[] s = hexString.Trim().Split(' '); //Trim先去掉前后空格，然后根据空格全部拆解，再组合。中间有多个空格也会去掉。
                StringBuilder sb = new StringBuilder();
                foreach (string item in s)
                {
                    if (item.Trim() != "")
                    {
                        sb.Append(item); //把为空的字符串组合起来
                    }
                }
                    string result = string.Empty;
                foreach (char c in sb.ToString())
                {
                    
                    int v = Convert.ToInt32(c.ToString(), 16);
                    int v2 = int.Parse(Convert.ToString(v, 2));
                    // 去掉格式串中的空格，即可去掉每个4位二进制数之间的空格，
                    result += string.Format("{0:d4} ", v2);
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

        }

    }
}
