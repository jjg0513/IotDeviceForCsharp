# More blinking lights, with hardware controls



The following elements are used in this sample:

* [Button](https://www.adafruit.com/product/367)

## Resources

* [Using .NET Core for IoT Scenarios](../README.md)
* [All about LEDs](https://learn.adafruit.com/all-about-leds)
