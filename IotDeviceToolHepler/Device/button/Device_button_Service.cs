﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.Pwm;
using System.Device.Pwm.Drivers;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Device.button
{
    /// <summary>
    ///     Capacitor    电容器
    /// </summary>
    public class Device_button_Service
    {
        //运行调用例子
        public void test()
        {
            FsrWithCapacitorSample();

        }
        private readonly GpioController _controller = new();
        private int _pinNumber = 18; // set the reading pin number

        public void FsrWithCapacitorSample()
        {
            _controller.OpenPin(_pinNumber);
        }

        public int ReadCapacitorChargingDuration()
        {
            int count = 0;
            _controller.SetPinMode(_pinNumber, PinMode.Output);
            // set pin to low
            _controller.Write(_pinNumber, PinValue.Low);
            // Prepare pin for input and wait until high read
            _controller.SetPinMode(_pinNumber, PinMode.Input);

            while (_controller.Read(_pinNumber) == PinValue.Low)
            {   // count until read High
                count++;
                if (count == 30000)
                {   // if count goes too high it means FSR in highest resistance which means no pressure, do not need to count more
                    break;
                }
            }
            return count;
        }
    }
}
