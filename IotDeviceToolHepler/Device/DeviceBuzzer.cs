﻿using Iot.Device.Buzzer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Device
{
    /// <summary>
    /// 蜂鸣器
    /// </summary>
    public class DeviceBuzzer
    {
        //运行调用例子
        public void test()
        {

            OpenNoSource();
            Thread.Sleep(10000);
            CloseNoSource();
            Thread.Sleep(10000);
        }
        private Buzzer BuzzerNoSource { get; set; }
        private Buzzer BuzzerSource { get; set; }

        /// <summary>
        /// 无源蜂鸣器BuzzerNoSource
        /// </summary>
        public void OpenNoSource()
        {
            Console.WriteLine(111);
            BuzzerNoSource = new Buzzer(17);
            BuzzerNoSource.StartPlaying(50);
        }
        public void CloseNoSource()
        {
            BuzzerNoSource = new Buzzer(17);
            BuzzerNoSource.StopPlaying();
        }
        /// <summary>
        ///  有源蜂鸣器BuzzerNoSource
        /// </summary>
        public void OpenSource()
        {
            BuzzerSource = new Buzzer(17);
            BuzzerSource.StartPlaying(50);
        }
        public void CloseSource()
        {
            BuzzerSource = new Buzzer(17);
            BuzzerSource.StopPlaying();
        }
    }
}
