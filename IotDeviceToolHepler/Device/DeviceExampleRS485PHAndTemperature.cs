﻿using Iot.Device.Pn532;
using IotDeviceToolHepler.Utils;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Device
{
    public class DeviceExampleRS485PHAndTemperature
    {

        /// <summary>
        /// RS485-PH值，温度检测 普锐森社
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="baudRate"></param>
        /// <param name="sendData"></param>
        /// <returns>ph,温度</returns>
        public Dictionary<string, double> getDataPHAndTemperature(string portName, int baudRate, string sendData = "")
        {  //普锐森社
            //RS485-PH检测 液体PR-*-PH-N01 温度
            //https://detail.tmall.com/item.htm?app=chrome&bxsign=scdZ9JpKY7KITx1r7c-sxVPULHKt39WeEXJKm_UD_AzFM7E6_9VCZILEVzj19F0Cz9jcjcLzoK41-epEbZnHlE2vjdvGiRD2dcq9azHggQNKYip69L3CaDIBD2XWwio5SSN&cpp=1&id=653395140353&share_crt_v=1&shareurl=true&short_name=h.Urta6qn&sourceType=item&sp_abtk=gray_1_code_simpleAndroid2&sp_tk=Wk1nd2RRM2ZqZHM=&spm=a2159r.13376460.0.0&tbSocialPopKey=shareItem&tk=ZMgwdQ3fjds%20CZ0001&ttid=1568860058617@taobao_android_10.22.10&un=87d1c8dd838bb7dde7c4cca5375f99dd&un_site=0&ut_sk=1.YSw0382wAckDAP1SKlIXnY+4_21646297_1679384494095.GoodsTitleURL.1
            //pH 计算：316H（十六进制）= 790 =>pH = 7.90
            //https://www.prsens.com/index.php?a=shows&catid=85&id=230
            Dictionary<string, double> keyValuePairs = new Dictionary<string, double>();
            string hex = new UtilsSerial().getPortData(portName, baudRate, "010300000002C40B");
            string dataHex = hex.Substring(6, 4);
            double ph = new UtilsDataTypeChange().hex16Convert10(dataHex) * 0.01;
            Console.WriteLine(ph);
            keyValuePairs.Add("ph", ph);

            //温度
            dataHex = hex.Substring(10, 4);
            double temperature = new UtilsDataTypeChange().hex16Convert10(dataHex) * 0.1;
            Console.WriteLine(temperature.ToString() + "℃");
            keyValuePairs.Add("temperature", temperature);
            return keyValuePairs;
        }


    }
}
