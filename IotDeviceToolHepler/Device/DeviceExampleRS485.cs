﻿using Iot.Device.Pn532;
using IotDeviceToolHepler.Utils;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Device
{
    public class DeviceExampleRS485
    {
        /// <summary>
        ///普锐森社 RS485光照温湿三合一(pr - 3002 - gzws - n01)
        /// </summary>
        /// <param name="portName">com4</param>
        /// <param name="baudRate">默认4800</param>
        /// <returns>光照，温度，湿度</returns>
        public Dictionary<string, double> getDataModelCodeAs1013(string portName, int baudRate)
        {
            #region
            //普锐森社
            //RS485光照温湿三合一(pr - 3002 - gzws - n01)
            //https://item.taobao.com/item.htm?id=565706395404&sourceType=item&ttid=1568860058617@taobao_android_10.22.10&ut_sk=1.YSw0382wAckDAP1SKlIXnY%2B4_21646297_1679303169101.GoodsTitleURL.1&un=87d1c8dd838bb7dde7c4cca5375f99dd&share_crt_v=1&un_site=0&spm=a2159r.13376460.0.0&sp_abtk=gray_1_code_simpleAndroid2&tbSocialPopKey=shareItem&sp_tk=a1hva2RRWkF5dlc%3D&cpp=1&shareurl=true&short_name=h.UJJdSix&bxsign=scdEgfsB8yi6ccFoo5Yf4KmOl7iCX3pS0Tzof4AxRsn4Vke336RGutceEyMVASJO92t7NVPE0Hod8wgod4LXYWrq6Ic0vEaeQERElVHmx3RS2NuAVagcWRzZV88EkBn3pm6&tk=kXokdQZAyvW%20CZ3457&app=chrome&sourceType=item&ttid=1568860058617@taobao_android_10.22.10&ut_sk=1.YSw0382wAckDAP1SKlIXnY%2B4_21646297_1679303169101.GoodsTitleURL.1&un=87d1c8dd838bb7dde7c4cca5375f99dd&share_crt_v=1&un_site=0&spm=a2159r.13376460.0.0&sp_abtk=gray_1_code_simpleAndroid2&tbSocialPopKey=shareItem&sp_tk=a1hva2RRWkF5dlc%3D&cpp=1&shareurl=true&short_name=h.UJJdSix&bxsign=scdEgfsB8yi6ccFoo5Yf4KmOl7iCX3pS0Tzof4AxRsn4Vke336RGutceEyMVASJO92t7NVPE0Hod8wgod4LXYWrq6Ic0vEaeQERElVHmx3RS2NuAVagcWRzZV88EkBn3pm6&tk=kXokdQZAyvW%20CZ3457&app=chrome
            //官网https://www.prsens.com/index.php?a=shows&catid=45&id=109

            Dictionary<string, double> keyValuePairs = new Dictionary<string, double>();
            //1）产品为0~65535量程变送器，单位为1Lux
            //0530 H(十六进制) = 1328=> 光照度 = 1328 Lux
            //2）产品为0~200000量程变送器，单位为百Lux
            //光照
            string lighthex = new UtilsSerial().getPortData(portName, baudRate, "010300060001640B");
            string lightHex = lighthex.Substring(6, 4);
            double light = new UtilsDataTypeChange().hex16Convert10(lightHex);
            Console.WriteLine(light + " lur");
            keyValuePairs.Add("light", light);

            //温度：当温度低于0℃时以补码形式上传
            //FF9B H(十六进制) = -101 => 温度 = -10.1℃
            //湿度：
            //292 H(十六进制) = 658=> 湿度 = 65.8 % RH
            //温湿
            string tAhhex = new UtilsSerial().getPortData(portName, baudRate, "010300000002C40B");
            //解析
            string humidityHex = tAhhex.Substring(6, 4);
            string temperatureHex = tAhhex.Substring(10, 4);
            double temperature = 0.0;
            double humidity = 0.0;
            string temperatureBinary = UtilsDataTypeChange.hexToBinaryTolStr(temperatureHex);
            humidity = new UtilsDataTypeChange().hex16Convert10(humidityHex) * 0.1;
            Console.WriteLine("humidity:" + humidity + "%RH");
            if (temperatureBinary.Substring(0, 1) == "0")
            {
                temperature = new UtilsDataTypeChange().hex16Convert10(temperatureHex) * 0.1;
            }
            else if (temperatureBinary.Substring(0, 1) == "1")
            {
                string bumabinary = UtilsByte.GetBuMaBinary(temperatureBinary);
                temperature = UtilsDataTypeChange.binaryToDecimalStr(bumabinary) * 0.1;
            }
            keyValuePairs.Add("temperature", temperature);
            keyValuePairs.Add("humidity", humidity);
            Console.WriteLine("temperature:" + temperature + "℃");
            return keyValuePairs;
            #endregion
        }
        /// <summary>
        /// RS485-PH值，温度检测 普锐森社
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="baudRate"></param>
        /// <param name="sendData"></param>
        /// <returns>ph,温度</returns>
        public Dictionary<string, double> getDataModelCodeAs1014(string portName, int baudRate, string sendData = "")
        {  //普锐森社
            //RS485-PH检测 液体PR-*-PH-N01 温度
            //https://detail.tmall.com/item.htm?app=chrome&bxsign=scdZ9JpKY7KITx1r7c-sxVPULHKt39WeEXJKm_UD_AzFM7E6_9VCZILEVzj19F0Cz9jcjcLzoK41-epEbZnHlE2vjdvGiRD2dcq9azHggQNKYip69L3CaDIBD2XWwio5SSN&cpp=1&id=653395140353&share_crt_v=1&shareurl=true&short_name=h.Urta6qn&sourceType=item&sp_abtk=gray_1_code_simpleAndroid2&sp_tk=Wk1nd2RRM2ZqZHM=&spm=a2159r.13376460.0.0&tbSocialPopKey=shareItem&tk=ZMgwdQ3fjds%20CZ0001&ttid=1568860058617@taobao_android_10.22.10&un=87d1c8dd838bb7dde7c4cca5375f99dd&un_site=0&ut_sk=1.YSw0382wAckDAP1SKlIXnY+4_21646297_1679384494095.GoodsTitleURL.1
            //pH 计算：316H（十六进制）= 790 =>pH = 7.90
            //https://www.prsens.com/index.php?a=shows&catid=85&id=230
            Dictionary<string, double> keyValuePairs = new Dictionary<string, double>();
            string hex = new UtilsSerial().getPortData(portName, baudRate, "010300000002C40B");
            string dataHex = hex.Substring(6, 4);
            double ph = new UtilsDataTypeChange().hex16Convert10(dataHex) * 0.01;
            Console.WriteLine(ph);
            keyValuePairs.Add("ph", ph);

            //温度
            dataHex = hex.Substring(10, 4);
            double temperature = new UtilsDataTypeChange().hex16Convert10(dataHex) * 0.1;
            Console.WriteLine(temperature.ToString() + "℃");
            keyValuePairs.Add("temperature", temperature);
            return keyValuePairs;
        }

        /// <summary>
        /// RS485-PH值，温度检测 普锐森社
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="baudRate"></param>
        /// <param name="sendData"></param>
        /// <returns>ph,温度</returns>
        public Dictionary<string, double> getDataModelCodeAs1015(string portName, int baudRate, string sendData = "")
        {  //普锐森社
            //RS485-ec检测 液体PR-*-PH-N01 温度
            //https://detail.tmall.com/item.htm?app=chrome&bxsign=scdZ9JpKY7KITx1r7c-sxVPULHKt39WeEXJKm_UD_AzFM7E6_9VCZILEVzj19F0Cz9jcjcLzoK41-epEbZnHlE2vjdvGiRD2dcq9azHggQNKYip69L3CaDIBD2XWwio5SSN&cpp=1&id=653395140353&share_crt_v=1&shareurl=true&short_name=h.Urta6qn&sourceType=item&sp_abtk=gray_1_code_simpleAndroid2&sp_tk=Wk1nd2RRM2ZqZHM=&spm=a2159r.13376460.0.0&tbSocialPopKey=shareItem&tk=ZMgwdQ3fjds%20CZ0001&ttid=1568860058617@taobao_android_10.22.10&un=87d1c8dd838bb7dde7c4cca5375f99dd&un_site=0&ut_sk=1.YSw0382wAckDAP1SKlIXnY+4_21646297_1679384494095.GoodsTitleURL.1
            //pH 计算：316H（十六进制）= 790 =>pH = 7.90
            //https://www.prsens.com/index.php?a=shows&catid=85&id=230
            Dictionary<string, double> keyValuePairs = new Dictionary<string, double>();
            string hex = new UtilsSerial().getPortData(portName, baudRate, "010300000002C40B");
            string dataHex = hex.Substring(6, 4);
            double ec = new UtilsDataTypeChange().hex16Convert10(dataHex) * 1.0;
            Console.WriteLine(ec);
            keyValuePairs.Add("ec", ec);

            //温度
            dataHex = hex.Substring(10, 4);
            double temperature = new UtilsDataTypeChange().hex16Convert10(dataHex) * 0.1;
            Console.WriteLine(temperature.ToString() + "℃");
            keyValuePairs.Add("temperature", temperature);
            return keyValuePairs;
        }

        /// <summary>
        /// 土壤温湿度传感器水分变送器 普锐森社
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="baudRate"></param>
        /// <param name="sendData"></param>
        /// <returns>ec,水份，ph,温度</returns>
        public Dictionary<string, double> getDataModelCodeAs1016(string portName, int baudRate, string sendData = "")
        {  //普锐森社
           //土壤温湿度传感器水分变送器农业大棚土质肥力氮磷钾酸碱度检测仪
           //https://detail.tmall.com/item.htm?app=chrome&bxsign=scdyIRcNhp2OlnAnDK2wwSsVyEhbXlIDj3SsPWtbpzYtQewW4l1_ret8XK7nVvu5kE7Mht8rAXi7k1nuzMZtv282S831EggqM6bCkF6DUNmyPtYYSnBwoXodSPKppjyNV5Q&cpp=1&id=593906558680&share_crt_v=1&shareurl=true&short_name=h.UJuuRon&sourceType=item&sp_abtk=gray_1_code_simpleAndroid2&sp_tk=bzFGdGRRZFJjWDk=&spm=a2159r.13376460.0.0&tbSocialPopKey=shareItem&tk=o1FtdQdRcX9%20CZ3457&ttid=1568860058617@taobao_android_10.22.10&un=87d1c8dd838bb7dde7c4cca5375f99dd&un_site=0&ut_sk=1.YSw0382wAckDAP1SKlIXnY+4_21646297_1679372679676.GoodsTitleURL.1
           //温度计算：
           //            当温度低于 0 ℃ 时温度数据以补码的形式上传。
           //温度：FF9B H(十六进制)= -101 => 温度 = -10.1℃
           //水分计算：
           //水分：292 H(十六进制) = 658 => 湿度 = 65.8 %，即土壤体积含水率为 65.8 %。
           //电导率计算：
           //电导率：3E8 H(十六进制) = 1000 电导率 = 1000 us / cm
           //PH 值计算：
           //PH 值：38H（十六进制）= 56 => PH 值 = 5.6
           //https://www.prsens.com/index.php?a=shows&catid=44&id=227


            Dictionary<string, double> keyValuePairs = new Dictionary<string, double>();
            string hex = new UtilsSerial().getPortData(portName, baudRate, "0103000000044409");
            string waterdataHex = hex.Substring(6, 4);//水份
            string temperdataHex = hex.Substring(10, 4);//温度值
            string ecdataHex = hex.Substring(14, 4);//电导率值 
            string phdataHex = hex.Substring(18, 4);//PH 值

            //水份
            double water = new UtilsDataTypeChange().hex16Convert10(waterdataHex) * 0.1;
            Console.WriteLine(water + "%");
            keyValuePairs.Add("water", water);
            //电导率值 
            double ec = new UtilsDataTypeChange().hex16Convert10(ecdataHex) * 1.0;
            Console.WriteLine(ec + "μs/cm");
            keyValuePairs.Add("ec", ec);
            //PH 值
            double ph = new UtilsDataTypeChange().hex16Convert10(phdataHex) * 0.1;
            Console.WriteLine(ph);
            keyValuePairs.Add("ph", ph);

            //温度
            double temperature = new UtilsDataTypeChange().hex16Convert10(temperdataHex) * 0.1;
            Console.WriteLine(temperature.ToString() + "℃");
            keyValuePairs.Add("temperature", temperature);
            return keyValuePairs;
        }
    }
}
