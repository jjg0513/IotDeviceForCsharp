﻿using Iot.Device.Sht3x;
using IotDeviceToolHepler.Model.Device;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Device.I2c;
using System.Text;

namespace IotDeviceToolHepler.Device
{
    /// <summary>
    ///  //sht30温湿传感器
    /// </summary>
    public class Sensor_SHT30
    {

        public void test()
        {
            //运行调用例子
            while (true)
            {
                ModelSht30Info returnValue = getTempAndHum(0);
                Thread.Sleep(1000);
            }
        }
        static Sht3x sht = null;
        public ModelSht30Info getTempAndHum(int busId)
        {
            if (sht == null)
            {
                I2cConnectionSettings set = new(busId, (byte)OverWriteI2cAddress.AddrLow);
                I2cDevice dev = I2cDevice.Create(set);
                sht = new Sht3x(dev);
            }
            ModelSht30Info returnValue = new ModelSht30Info();
            // 温度
            returnValue.temperature = sht.Temperature.DegreesCelsius;
            // 湿度
            returnValue.humidity = sht.Humidity.Percent;
            Console.WriteLine("温度：{0:N1} ℃\n湿度：{1:N1} %RH", returnValue.temperature, returnValue.humidity);
            return returnValue; ;
        }
        enum OverWriteI2cAddress : byte
        {
            AddrLow = 0x44
        }
    }


}
