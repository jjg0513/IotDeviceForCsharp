﻿using Iot.Device.CharacterLcd;
using Iot.Device.Ili9341;
using Iot.Device.Pcx857x;
using IotDeviceToolHepler.Model.device;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.I2c;
using System.Device.Spi;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Device
{
    /// <summary>
    /// 显示设备
    /// </summary>
    public class DeviceDisplay_Lcdili9341
    {
        /// <summary>
        /// 测试用
        /// </summary>
        public void Test()
        {
            while (true)
            {
                Lcdili9341();
                Thread.Sleep(1000);
            }
        }



        /// <summary>
        /// lCDili9341显示屏spi
        /// </summary>
        public void Lcdili9341()
        {

            const int pinID_DC = 25;
            const int pinID_Reset = 24;

            using Bitmap dotnetBM = new(240, 320);
            using Graphics g = Graphics.FromImage(dotnetBM);
            using SpiDevice displaySPI = SpiDevice.Create(new SpiConnectionSettings(0, 0) { Mode = SpiMode.Mode3, DataBitLength = 8, ClockFrequency = 12_000_000 /* 12MHz */ });
            using Ili9341 ili9341 = new(displaySPI, pinID_DC, pinID_Reset);

            while (true)
            {
                foreach (string filepath in Directory.GetFiles(@"images", "*.png").OrderBy(f => f))
                {
                    using Bitmap bm = (Bitmap)Image.FromFile(filepath);
                    g.Clear(Color.Black);
                    g.DrawImageUnscaled(bm, 0, 0);
                    ili9341.SendBitmap(dotnetBM);
                    Task.Delay(1000).Wait();
                }
            }
            //代码来源https://github.com/dotnet/iot/blob/main/src/devices/Ili9341/README.md
        }
    }
}
