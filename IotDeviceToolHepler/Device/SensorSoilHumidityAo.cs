﻿using IotDeviceToolHepler.Model.Constant;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.I2c;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Device
{
    /// <summary>
    /// 土壤湿度传感器
    /// </summary>
    public static class SensorSoilHumidityAo
    {
        //运行调用例子
        public static void Test()
        {
            while (true)
            {
                byte v = GetSoilHumidity();
                Thread.Sleep(1000);
            }
        }
        public static byte GetSoilHumidity()
        {

            byte i2cAddrressByte = (byte)Convert.ToInt32("0x48", 16);//16进制转10进制
                                                                     //AO模拟转数字强度值
            I2cConnectionSettings set = new(1, i2cAddrressByte);
            I2cDevice dev = I2cDevice.Create(set);
            dev.WriteByte((byte)ConstantPCF8591.ch1);
            byte v = dev.ReadByte();
            Console.WriteLine("湿度强度值：" + v);
            return v;
        }

    }
}
