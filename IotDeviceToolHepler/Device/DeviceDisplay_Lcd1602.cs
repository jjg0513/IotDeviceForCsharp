﻿using Iot.Device.CharacterLcd;
using Iot.Device.Ili9341;
using Iot.Device.Pcx857x;
using IotDeviceToolHepler.Model.device;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.I2c;
using System.Device.Spi;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Device
{
    /// <summary>
    /// 显示设备 Lcd1602显示屏Pcf8574驱动芯片
    /// </summary>
    public class DeviceDisplay_Lcd1602
    {
        /// <summary>
        /// 测试用Lcd1602显示屏Pcf8574驱动芯片
        /// </summary>
        public void Test()
        {
            while (true)
            {
                List<ModelWriteLcd1602> modelWriteLcd1602s = new List<ModelWriteLcd1602>();
                ModelWriteLcd1602 modelWriteLcd1602 = new ModelWriteLcd1602();
                modelWriteLcd1602.content = DateTime.Now.ToString("MM-dd HH:mm:ss");//显示内容
                modelWriteLcd1602.row = 0;//第几行显示
                modelWriteLcd1602.left = 1;//第几列显示
                modelWriteLcd1602s.Add(modelWriteLcd1602);
                Write(3, modelWriteLcd1602s);
                Thread.Sleep(1000);
            }
        }
        static Lcd1602 lcd = null;
        /// <summary>
        /// 基于Pcf8574 Lcd1602显示屏i2c  方式2 适配树莓派 busId=1、香橙派 busId=3，中文会乱码
        /// </summary>
        /// <param name="busId">总线Id</param>
        /// <param name="content">内容</param>
        /// <param name="left">共16列，第几列显示</param>
        /// <param name="line">共2行，在第几行显示</param>
        public static void Write(int busId, List<ModelWriteLcd1602> modelWriteLcd1602s)
        {
            if (lcd == null)
            {
                var i2cDevice = I2cDevice.Create(new I2cConnectionSettings(busId: busId, deviceAddress: 0x27));
                var controller = new Pcf8574(i2cDevice);
                lcd = new Lcd1602(i2cDevice, false);
            }
            lcd.Clear();
            foreach (ModelWriteLcd1602 item in modelWriteLcd1602s)
            {
                lcd.SetCursorPosition(item.left, item.row);
                lcd.Write(item.content);
            }
        }
        /// <summary>
        /// 基于Pcf8574 Lcd1602显示屏i2c  方式2 适配树莓派 busId=1、香橙派 busId=3,支持字母数字，中文会乱码 
        /// </summary>
        /// <param name="busId">总线Id</param>
        /// <param name="content">内容</param>
        /// <param name="left">共16列，第几列显示</param>
        /// <param name="line">共2行，在第几行显示</param>
        public void Lcd1602Method2(int busId, string content, int left = 0, int line = 0)
        {
            Console.WriteLine("Displaying current time. Press Ctrl+C to end.");
            using I2cDevice i2c = I2cDevice.Create(new I2cConnectionSettings(busId, 0x27));
            using var driver = new Pcf8574(i2c);
            using var lcd = new Lcd1602(registerSelectPin: 0,
                                    enablePin: 2,
                                    dataPins: new int[] { 4, 5, 6, 7 },
                                    backlightPin: 3,
                                    backlightBrightness: 0.1f,
                                    readWritePin: 1,
                                    controller: new GpioController(PinNumberingScheme.Logical, driver));
            lcd.Clear();
            lcd.SetCursorPosition(left, line);
            lcd.Write(content);
        }


    }
}
