﻿using Iot.Device.Pn532;
using IotDeviceToolHepler.Utils;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Device
{
    public class DeviceExampleRS485Soil
    {

        /// <summary>
        /// 土壤温湿度传感器水分变送器 普锐森社
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="baudRate"></param>
        /// <param name="sendData"></param>
        /// <returns>ec,水份，ph,温度</returns>
        public Dictionary<string, double> getDataSoil(string portName, int baudRate, string sendData = "")
        {  //普锐森社
           //土壤温湿度传感器水分变送器农业大棚土质肥力氮磷钾酸碱度检测仪
           //https://detail.tmall.com/item.htm?app=chrome&bxsign=scdyIRcNhp2OlnAnDK2wwSsVyEhbXlIDj3SsPWtbpzYtQewW4l1_ret8XK7nVvu5kE7Mht8rAXi7k1nuzMZtv282S831EggqM6bCkF6DUNmyPtYYSnBwoXodSPKppjyNV5Q&cpp=1&id=593906558680&share_crt_v=1&shareurl=true&short_name=h.UJuuRon&sourceType=item&sp_abtk=gray_1_code_simpleAndroid2&sp_tk=bzFGdGRRZFJjWDk=&spm=a2159r.13376460.0.0&tbSocialPopKey=shareItem&tk=o1FtdQdRcX9%20CZ3457&ttid=1568860058617@taobao_android_10.22.10&un=87d1c8dd838bb7dde7c4cca5375f99dd&un_site=0&ut_sk=1.YSw0382wAckDAP1SKlIXnY+4_21646297_1679372679676.GoodsTitleURL.1
           //温度计算：
           //            当温度低于 0 ℃ 时温度数据以补码的形式上传。
           //温度：FF9B H(十六进制)= -101 => 温度 = -10.1℃
           //水分计算：
           //水分：292 H(十六进制) = 658 => 湿度 = 65.8 %，即土壤体积含水率为 65.8 %。
           //电导率计算：
           //电导率：3E8 H(十六进制) = 1000 电导率 = 1000 us / cm
           //PH 值计算：
           //PH 值：38H（十六进制）= 56 => PH 值 = 5.6
           //https://www.prsens.com/index.php?a=shows&catid=44&id=227
           //01 03 00 00 00 04 44 09 原来
           //02 03 00 00 00 04 44 3A  修改了后地址位2
            Dictionary<string, double> keyValuePairs = new Dictionary<string, double>();
            string hex = new UtilsSerial().getPortData(portName, baudRate, "0103000000044409");
            string waterdataHex = hex.Substring(6, 4);//水份
            string temperdataHex = hex.Substring(10, 4);//温度值
            string ecdataHex = hex.Substring(14, 4);//电导率值 
            string phdataHex = hex.Substring(18, 4);//PH 值

            //水份
            double water = new UtilsDataTypeChange().hex16Convert10(waterdataHex) * 0.1;
            Console.WriteLine(water + "%");
            keyValuePairs.Add("water", water);
            //电导率值 
            double ec = new UtilsDataTypeChange().hex16Convert10(ecdataHex) * 1.0;
            Console.WriteLine(ec + "μs/cm");
            keyValuePairs.Add("ec", ec);
            //PH 值
            double ph = new UtilsDataTypeChange().hex16Convert10(phdataHex) * 0.1;
            Console.WriteLine(ph);
            keyValuePairs.Add("ph", ph);

            //温度
            double temperature = new UtilsDataTypeChange().hex16Convert10(temperdataHex) * 0.1;
            Console.WriteLine(temperature.ToString() + "℃");
            keyValuePairs.Add("temperature", temperature);
            return keyValuePairs;
        }
    }
}
