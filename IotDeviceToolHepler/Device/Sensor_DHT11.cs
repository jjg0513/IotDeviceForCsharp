﻿using Iot.Device.DHTxx;
using IotDeviceToolHepler.Model.Device;
using UnitsNet;

namespace IotDeviceToolHepler.Device
{
    /// <summary>
    ///  //dht11温湿传感器
    /// </summary>
    public static class Sensor_DHT11
    {

        public static void test()
        {
            //运行调用例子
            while (true)
            {
                ModelDht11Info returnValue = getTempAndHum();
                Thread.Sleep(1000);
            }
        }
        public static ModelDht11Info getTempAndHum()
        {
            ModelDht11Info dht11Info = new ModelDht11Info();
            Dht11 dht11 = new Dht11(16);
            Temperature temperature;
            dht11.TryReadTemperature(out temperature);
            Console.WriteLine("Temperature:" + temperature.DegreesCelsius);
            RelativeHumidity relativeHumidity;
            dht11.TryReadHumidity(out relativeHumidity);
            Console.WriteLine("Humidity:" + relativeHumidity.Percent);
            dht11Info.Temperature = temperature.DegreesCelsius;
            dht11Info.Humidity = relativeHumidity.Percent;
            return dht11Info;
        }
    }


}
