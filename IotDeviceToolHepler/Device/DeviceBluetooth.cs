﻿
using System;
using InTheHand.Net.Sockets;
using InTheHand;
using InTheHand.Net.Bluetooth;
using System.Threading;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace IotDeviceToolHepler.Device
{
    public class DeviceBluetooth
    {

        //Device_Bluetooth
        //{
        //    bluetoothClient.Close();
        //}

        BluetoothClient bluetoothClient;
        //static BluetoothListener bluetoothListener;
        //static Thread listenThread;
        static bool isConnected;
        public void Test()
        {

            GetDeviceList();

            //listenThread = new Thread(ReceiveData);
            //listenThread.IsBackground = true;

            //listenThread.Start();
            Console.ReadKey();
        }

        //BluetoothService

        public bool GetDeviceList()
        {
            Console.WriteLine("Finding Devides...");
            //BluetoothRadio.PrimaryRadio.Mode = InTheHand.Net.Bluetooth.RadioMode.Connectable;
            BluetoothClient cli = new BluetoothClient();
            IReadOnlyCollection<BluetoothDeviceInfo> devices = cli.DiscoverDevices();
            foreach (BluetoothDeviceInfo device in devices) //设备搜寻           
            {
                //device.Update();
                device.Refresh();
                if (device.DeviceName == "mark_1")
                {
                    Console.WriteLine("Connecting...");
                    try
                    {
                        if (!device.Connected)
                        {
                            cli.Connect(device.DeviceAddress, BluetoothService.SerialPort);
                            Console.WriteLine("Connected!");

                            bluetoothClient = cli;
                            ReceiveData();
                        }
                        else
                            Console.WriteLine("Has Been Connected!");
                    }
                    catch (Exception e)
                    { Console.WriteLine("Failed:" + e); }

                    break;
                }
            }

            //Thread ReceiveThread = new Thread(ReceiveData);
            //ReceiveThread.Start();
            return true;
        }

        private void ReceiveData()
        {
            isConnected = bluetoothClient.Connected;
            while (isConnected)
            {
                try
                {
                    string receive = string.Empty;

                    Stream peerStream = bluetoothClient.GetStream();
                    byte[] buffer = new byte[255];
                    peerStream.Read(buffer, 0, 255);
                    receive = Encoding.UTF8.GetString(buffer).ToString().Replace("\0", "");

                    //Console.ReadKey();
                    Console.Write(receive);
                }
                catch (Exception e)
                {
                    Console.Write("Error:" + e);
                    break;
                }
            }
        }
    }
}

