﻿using Iot.Device.DCMotor;
using Iot.Device.Uln2003;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.Pwm;
using System.Device.Pwm.Drivers;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Iot.Device.ExplorerHat;

namespace IotDeviceToolHepler.Device
{

    /// <summary>
    /// l298N高电压的电机驱动器
    /// </summary>
    public class Device_StepperL298N
    {

        /// <summary>
        /// 调用示例
        /// </summary>
        public void test()
        {
            while (true)
            {
                Console.WriteLine("输入b逆时针转，输入f顺时针转");
                //控制台中输入内容
                string key = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(key))
                {
                    if (key == "b")
                    {
                        back();
                    }
                    else
                    {
                        ford();
                    }

                }
            }

        }
        public void stop1298()
        {
            var pwm = new SoftwarePwmChannel(26);
            DCMotor = DCMotor.Create(pwm);
            DCMotor.Stop();
        }
        private DCMotor DCMotor { get; set; }
        private PwmChannel BackChannel;
        private PwmChannel ForChannel;
        public void deviceStepperL298N()
        {
            BackChannel = new SoftwarePwmChannel(19);
            ForChannel = new SoftwarePwmChannel(26);
            ForChannel.Start();
            BackChannel.Start();
        }

        /// <summary>
        /// l298N电机驱动，顺时针方向转
        /// </summary>
        public void back()
        {
            ForChannel.Stop();
            ForChannel.Dispose();
            Thread.Sleep(200);
            BackChannel.Start();
            BackChannel.DutyCycle = 1;
        }
        /// <summary>
        /// l298N电机驱动，逆针方向转
        /// </summary>
        public void ford()
        {
            BackChannel.Stop();
            BackChannel.Dispose();
            Thread.Sleep(200);
            ForChannel.Start();
            ForChannel.DutyCycle = 1;
        }
    }
}
