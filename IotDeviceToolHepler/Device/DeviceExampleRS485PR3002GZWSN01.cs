﻿using Iot.Device.Pn532;
using IotDeviceToolHepler.Utils;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Device
{
    public class DeviceExampleRS485PR3002GZWSN01
    {

        /// <summary>
        ///普锐森社 RS485光照温湿三合一(pr - 3002 - gzws - n01)
        /// </summary>
        /// <param name="portName">com4</param>
        /// <param name="baudRate">默认4800</param>
        /// <returns>光照，温度，湿度</returns>
        public Dictionary<string, double> getDataPR3002GZWSN01(string portName, int baudRate)
        {
            #region
            //普锐森社
            //RS485光照温湿三合一(pr - 3002 - gzws - n01)
            //https://item.taobao.com/item.htm?id=565706395404&sourceType=item&ttid=1568860058617@taobao_android_10.22.10&ut_sk=1.YSw0382wAckDAP1SKlIXnY%2B4_21646297_1679303169101.GoodsTitleURL.1&un=87d1c8dd838bb7dde7c4cca5375f99dd&share_crt_v=1&un_site=0&spm=a2159r.13376460.0.0&sp_abtk=gray_1_code_simpleAndroid2&tbSocialPopKey=shareItem&sp_tk=a1hva2RRWkF5dlc%3D&cpp=1&shareurl=true&short_name=h.UJJdSix&bxsign=scdEgfsB8yi6ccFoo5Yf4KmOl7iCX3pS0Tzof4AxRsn4Vke336RGutceEyMVASJO92t7NVPE0Hod8wgod4LXYWrq6Ic0vEaeQERElVHmx3RS2NuAVagcWRzZV88EkBn3pm6&tk=kXokdQZAyvW%20CZ3457&app=chrome&sourceType=item&ttid=1568860058617@taobao_android_10.22.10&ut_sk=1.YSw0382wAckDAP1SKlIXnY%2B4_21646297_1679303169101.GoodsTitleURL.1&un=87d1c8dd838bb7dde7c4cca5375f99dd&share_crt_v=1&un_site=0&spm=a2159r.13376460.0.0&sp_abtk=gray_1_code_simpleAndroid2&tbSocialPopKey=shareItem&sp_tk=a1hva2RRWkF5dlc%3D&cpp=1&shareurl=true&short_name=h.UJJdSix&bxsign=scdEgfsB8yi6ccFoo5Yf4KmOl7iCX3pS0Tzof4AxRsn4Vke336RGutceEyMVASJO92t7NVPE0Hod8wgod4LXYWrq6Ic0vEaeQERElVHmx3RS2NuAVagcWRzZV88EkBn3pm6&tk=kXokdQZAyvW%20CZ3457&app=chrome
            //官网https://www.prsens.com/index.php?a=shows&catid=45&id=109

            Dictionary<string, double> keyValuePairs = new Dictionary<string, double>();
            //1）产品为0~65535量程变送器，单位为1Lux
            //0530 H(十六进制) = 1328=> 光照度 = 1328 Lux
            //2）产品为0~200000量程变送器，单位为百Lux
            //光照
            string lighthex = new UtilsSerial().getPortData(portName, baudRate, "010300060001640B");
            string lightHex = lighthex.Substring(6, 4);
            double light = new UtilsDataTypeChange().hex16Convert10(lightHex);
            Console.WriteLine(light + " lur");
            keyValuePairs.Add("light", light);

            //温度：当温度低于0℃时以补码形式上传
            //FF9B H(十六进制) = -101 => 温度 = -10.1℃
            //湿度：
            //292 H(十六进制) = 658=> 湿度 = 65.8 % RH
            //温湿
            string tAhhex = new UtilsSerial().getPortData(portName, baudRate, "010300000002C40B");
            //解析
            string humidityHex = tAhhex.Substring(6, 4);
            string temperatureHex = tAhhex.Substring(10, 4);
            double temperature = 0.0;
            double humidity = 0.0;
            string temperatureBinary = UtilsDataTypeChange.hexToBinaryTolStr(temperatureHex);
            humidity = new UtilsDataTypeChange().hex16Convert10(humidityHex) * 0.1;
            Console.WriteLine("humidity:" + humidity + "%RH");
            if (temperatureBinary.Substring(0, 1) == "0")
            {
                temperature = new UtilsDataTypeChange().hex16Convert10(temperatureHex) * 0.1;
            }
            else if (temperatureBinary.Substring(0, 1) == "1")
            {
                string bumabinary = UtilsByte.GetBuMaBinary(temperatureBinary);
                temperature = UtilsDataTypeChange.binaryToDecimalStr(bumabinary) * 0.1;
            }
            keyValuePairs.Add("temperature", temperature);
            keyValuePairs.Add("humidity", humidity);
            Console.WriteLine("temperature:" + temperature + "℃");
            return keyValuePairs;
            #endregion
        }

    }
}
