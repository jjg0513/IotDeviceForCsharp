﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Model.Constant
{
    /// <summary>
    /// 模块的四个AIN串口   或p4 0,p5 1
    /// </summary>
    public enum ConstantPCF8591 : byte
    {
        ch1 = 0x40,
        ch2 = 0x41,
        ch3 = 0x42,
        ch4 = 0x43
    }

}
