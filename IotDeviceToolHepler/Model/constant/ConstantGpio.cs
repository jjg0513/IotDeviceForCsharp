﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Model.Constant
{
    public class ConstantGpio
    {
        /// <summary>
        /// 开
        /// </summary>
        public const string GpioPinOpen = "open";
        /// <summary>
        /// 关
        /// </summary>
        public const string GpioPinClose = "close";


        /// <summary>
        /// 关
        /// </summary>
        public const string GpioPinError = "error";


    }
}
