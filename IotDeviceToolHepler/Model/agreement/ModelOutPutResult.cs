﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Model.agreement
{
    public class ModelOutPutResult
    {

        /// <summary>
        /// 名称
        /// </summary>
        public string key { get; set; }
        /// <summary>
        /// 数据
        /// </summary>
        public object value { get; set; }
    }
}
