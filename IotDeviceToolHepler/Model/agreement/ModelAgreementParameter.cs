﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Model.agreement
{
    public class ModelAgreementParameter
    {
        /// <summary>
        /// gpio引脚wpi编号
        /// </summary>
        public int wpi { get; set; }

        /// <summary>
        /// 端口
        /// </summary>
        public string portName { get; set; }
        /// <summary>
        /// 波特率
        /// </summary>
        public int baudRate { get; set; }
        /// <summary>
        /// 总线通道
        /// </summary>
        public int busId { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public int address { get; set; }

        /// <summary>
        /// 通道
        /// </summary>
        public int channel { get; set; }

        public string data { get; set; }


    }
}
