﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Model.device
{
    //显示屏显示内容
    public class ModelWriteLcd1602
    {     

        /// <summary>
        /// 第几行
        /// </summary>
        public int row { get; set; }
        /// <summary>
        /// 左边第几位开始
        /// </summary>
        public int left { get; set; }
        /// <summary>
        /// 显示内容
        /// </summary>
        public string content { get; set; }
    }
}
