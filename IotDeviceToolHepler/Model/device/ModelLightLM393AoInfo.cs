﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Model.Device
{
    public class ModelLightLM393AoInfo
    {
        /// <summary>
        /// 模拟转数字
        /// </summary>
        public byte lightAo { get; set; }

        /// <summary>
        /// 阀值1黑，0亮
        /// </summary>
        public int lightState { get; set; }

    }
}
