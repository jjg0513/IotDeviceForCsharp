﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.Model.Device
{
    public class ModelSht30Info
    {
        public double temperature { get; set; }
        public double humidity { get; set; }
        /// <summary>
        /// 通讯协议 i2c/rs485
        /// </summary>
        public string agreementType { get; set; }

    }
}
