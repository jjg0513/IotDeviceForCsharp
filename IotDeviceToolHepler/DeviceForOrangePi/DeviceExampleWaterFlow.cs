﻿using Iot.Device.Gpio.Drivers;
using Iot.Device.Pn532;
using IotDeviceToolHepler.Model.Constant;
using IotDeviceToolHepler.Utils;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.DeviceForOrangePi
{
    /// <summary>
    /// 水流速度检测
    /// </summary>
    public class DeviceExampleWaterFlow
    {
        /// <summary>
        /// 获取YF-S201某引脚脉冲数量 ，霍尔水流量计 流量=脉冲数/7.5= L/min平均流量，累计=平均流量*时间(秒）
        /// </summary>
        /// <param name="pinNum">引脚wpi编号</param>
        ///<param name="second">秒数</param>
        /// <returns>返回second秒的脉冲数</returns>
        public int getPulseCount_YFS201(int pinNum, int second)
        {
            string old = new UtilsOrangePiGpio().getGpioInputMode(pinNum);
            int count = 0;
            count = 0;
            string resultold = old;
            DateTime dt = DateTime.Now.AddSeconds(second);
          
            while (dt > DateTime.Now)
            {
                string result = new UtilsOrangePiGpio().getGpioInputMode(pinNum);

                if (resultold != result)
                {
                    resultold = result;
                    if (old == result)
                    {
                        count = count + 1;
                    }
                }
                Thread.Sleep(1);//1000次、秒
                //Console.WriteLine(second + "秒脉冲数：" + count + " " + DateTime.Now);
            }
            //Console.WriteLine(second + "秒脉冲数：" + count + " " + DateTime.Now);
            return count;
        }

        public int pluseCount = 0;

        /// <summary>
        /// 微软的方法，不准 获取YF-S201某引脚脉冲数量 ，霍尔水流量计 流量=脉冲数/7.5= L/min平均流量，累计=平均流量*时间(秒）
        /// </summary>
        /// <param name="pinNum"></param>
        /// <param name="second"></param>
        public void getPulseCount_YFS201_V2(int pinNum)
        {
            pluseCount = 0;
            GpioController gpioController = new GpioController(PinNumberingScheme.Logical,new OrangePiZero2Driver());
            if (!gpioController.IsPinOpen(pinNum))
            {
                gpioController.OpenPin(pinNum);
            }

            gpioController.SetPinMode(pinNum, PinMode.Input);
            gpioController.RegisterCallbackForPinValueChangedEvent(pinNum, PinEventTypes.Rising, callback);           
        }

        public void callback(object sender, PinValueChangedEventArgs pinValueChangedEventArgs)
        {
            pluseCount = pluseCount + 1;
            Console.WriteLine(" 回调 " + pinValueChangedEventArgs.PinNumber + "--" + pinValueChangedEventArgs.ChangeType + "-" + System.DateTime.Now);
        }

        /// <summary>
        /// wiringOP的回调方法，准确，但一秒多超过10次变化，会报错退出程序，获取YF-S201某引脚脉冲数量 ，霍尔水流量计 流量=脉冲数/7.5= L/min平均流量，累计=平均流量*时间(秒）
        /// </summary>
        /// <param name="pinNum"></param>
        /// <param name="second"></param>
        public void getPulseCount_YFS201_V3(int pinNum)
        {
            Setup.WiringPiPiSetup();
            Interrupts.WiringPiPiISR(pinNum, WiringPi.IntEdgeRising, callbackPI);
        }

        public void callbackPI()
        {
            pluseCount = pluseCount + 1;
            Console.WriteLine(" 回调 " + System.DateTime.Now);
        }
    }
}
