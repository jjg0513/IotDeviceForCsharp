﻿using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.Pwm;
using System.Device.Pwm.Drivers;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IotDeviceToolHepler.DeviceForOrangePi
{
    /// <summary>
    /// 香橙子调用WiringOP操作gpio Output 高低电平
    /// </summary>
    public class DeviceLed3color
    {
        //运行调用例子
        public static void test()
        {

            gpioLed5V();

        }
        /// <summary>
        /// 方法1： pin输入高电平或低电平
        /// </summary>
        /// <param name="command"></param>
        /// <param name="pinNum"></param>
        public static void gpioOutputMode()
        {
            Setup.WiringPiPiSetup();
            GPIO.PinMode(17, WiringPi.Output);
            GPIO.PinMode(27, WiringPi.Output);
            GPIO.PinMode(22, WiringPi.Output);

            GPIO.DigitalWrite(17, WiringPi.High);
            GPIO.DigitalWrite(27, WiringPi.High);
            GPIO.DigitalWrite(22, WiringPi.High);
            Console.WriteLine("执行完成---GpioOutputMode");



        }

        /// <summary>
        ///方法2： pin输入高电平或低电平
        /// </summary>
        /// <param name="command"></param>
        /// <param name="pinNum"></param>
        public static void gpioLed5V()
        {
            GpioController gpioController = new GpioController();
            gpioController.OpenPin(17, PinMode.Output);
            gpioController.OpenPin(27, PinMode.Output);
            gpioController.OpenPin(18, PinMode.Output);


            gpioController.Write(17, PinValue.High);
            Thread.Sleep(1000);
            gpioController.Write(27, PinValue.High);
            Thread.Sleep(1000);
            gpioController.Write(18, PinValue.High);
            Thread.Sleep(1000);
            gpioController.Write(17, PinValue.Low);
            Thread.Sleep(1000);
            gpioController.Write(27, PinValue.Low);
            Thread.Sleep(1000);
            gpioController.Write(18, PinValue.Low);
            Thread.Sleep(1000);
            Console.WriteLine("执行完成---GpioLed5V");
        }

        /// <summary>
        ///方法3： 只对硬件pwm口有效，但pwm口并不多，只有1两个,实现不同的亮度
        /// </summary>
        /// <param name="command"></param>
        /// <param name="pinNum"></param>
        public static void gpioPwnPim()
        {
            //第一路 PWM 是引脚 GPIO 12(无效） 和 GPIO 18，板上的序号是 32、12；
            //第二路 PWM 是引脚 GPIO 13 和 GPIO 19，板上的序号是 33、35。
            //chip 参数常为 0，即第一块控制芯片，channel 是第几路PWM通道，树莓派有两路，所以此参数可以用 0 或 1。本次咱们用的是第一路，所以值是 0。frequency 是频率，默认400 Hz。dutyCyclePercentage 是占空比，0.0 到 1.0，表示 0% 到 100%，默认 0.5（50%）。
            //使用 Linux 自带的 Nano 应用打开 config.txt 文件。
            //sudo nano /boot/config.txt
            //最后加dtoverlay=pwm
            //按 Ctrl + X，退出，nano 提示是否保存，输入 Y，搞定。
            int brightness = 0;
            PwmChannel pwm = PwmChannel.Create(chip: 0, channel: 0, frequency: 400, dutyCyclePercentage: 0.5);
            pwm.Start();

            while (brightness != 255)
            {
                pwm.DutyCycle = brightness / 255D;

                brightness++;
                Thread.Sleep(10);
            }

            while (brightness != 0)
            {
                pwm.DutyCycle = brightness / 255D;

                brightness--;
                Thread.Sleep(10);
            }
            pwm.Stop();
            Console.WriteLine("执行完成---GpioPwnPim");
        }
        /// <summary>
        ///方法4(推荐）： 由于硬件的pwm口只有goip18不够用， 软件定义多几个 PWM口 Gpio SoftwarePwmChannel,实现不同的亮度
        /// </summary>
        public static void gpioSoftwarePwmChannel()
        {

            using PwmChannel red = new SoftwarePwmChannel(pinNumber: 17, frequency: 400, dutyCycle: 0);
            using PwmChannel green = new SoftwarePwmChannel(pinNumber: 27, frequency: 400, dutyCycle: 0);
            using PwmChannel blue = new SoftwarePwmChannel(pinNumber: 18, frequency: 400, dutyCycle: 0);

            red.Start();
            green.Start();
            blue.Start();

            breath(red, green, blue);

            red.Stop();
            green.Stop();
            blue.Stop();
            Console.WriteLine("执行完成---GpioSoftwarePwmChannel");
        }
        public static void breath(PwmChannel red, PwmChannel green, PwmChannel blue)
        {

            int r = 255, g = 0, b = 0;

            while (r != 0 && g != 255)
            {
                red.DutyCycle = r / 255D;
                green.DutyCycle = g / 255D;

                r--;
                g++;
                Thread.Sleep(10);
            }

            while (g != 0 && b != 255)
            {
                green.DutyCycle = g / 255D;
                blue.DutyCycle = b / 255D;

                g--;
                b++;
                Thread.Sleep(10);
            }

            while (b != 0 && r != 255)
            {
                blue.DutyCycle = b / 255D;
                red.DutyCycle = r / 255D;

                b--;
                r++;
                Thread.Sleep(10);
            }

        }
        /// <summary>
        ///方法5
        /// </summary>
        public void manyColorLED()
        {
            GpioController gpioController = new GpioController();
            var LIST = new List<int>() { 13, 19, 26 };
            for (int i = 0; i < LIST.Count; i++)
            {
                gpioController.OpenPin(LIST[i], PinMode.Output);
            }
            while (true)
            {
                for (int i = 0; i < LIST.Count; i++)
                {
                    gpioController.Write(LIST[i], PinValue.High);
                    Thread.Sleep(200);
                    gpioController.Write(LIST[i], PinValue.Low);
                }
            }
        }
    }
}
