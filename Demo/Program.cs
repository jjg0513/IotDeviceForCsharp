﻿using Demo.Example;
using IotDeviceToolHepler;
using IotDeviceToolHepler.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //gpio操作
             //new DemoPinMode().run();
            //serial 串口，使用wiringop类库
            //DemoSerial.Run();
            //serial DemoByIOPort  串口 使用SerialPort类库 
            //DemoByIOPort.Run();
            //土壤湿度传感器
            //Device_Sensor_SoilhumiditySevice.test();
            ////sht30环境温湿度传感器
            //Device_Sensor_SHT30TempAndHumService.test();
            ////485光感传感器
            //Device_Sensor_Light485WriteByteService.test(); 
            //gipo 高低电平操作
            //new OrangePi_GpioService().test();
            //显示器
            // DemoDisplay1602.Run();
            new DemoSerial().test();
          //  new DemoPwm().run();
        }
    }
}
