﻿using IotDeviceToolHepler.Utils;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Demo.Example
{

    /// <summary>
    /// 调用wiringPO操作串口
    /// </summary>
    public class DemoSerial
    {///usr/software/dotnet /usr/dotnet/Demo/Demo.dll
        public void test()
        {
            // string reslut = "01 04 30 00 00 00 00 00 2F 00 00 00 00 00 3E 00 00 00 00 00 4C 00 00 00 00 00 7C 00 00 00 00 00 1A 00 00 00 00 00 12 00 00 00 00 00 0F 00 00 00 00 00 27 CF 89";
            // Console.WriteLine(reslut.Substring(15,11));
            //Console.WriteLine(reslut.Substring(33, 11));
            //Console.WriteLine(reslut.Substring(51, 11));
            //Console.WriteLine(reslut.Substring(69, 11));
            //Console.WriteLine(reslut.Substring(87, 11));
            //Console.WriteLine(reslut.Substring(105, 11));
            //Console.WriteLine(reslut.Substring(123, 11));
            //Console.WriteLine(reslut.Substring(141, 11));
            //Console.WriteLine(reslut.Length);
            //Console.WriteLine("----------22222222222222------------");
            //return;
            while (true)
            {
                
                Console.WriteLine(new UtilsSerial().getPortData("COM4", 9600, "01 04 00 00 00 18 F0 00"));

                Console.WriteLine("");
                Console.WriteLine("");
                Thread.Sleep(1000);
            }
            //Task.Run(() =>
            //{
            //    while (true)
            //    {
            //        Console.WriteLine(System.DateTime.Now);
            //        Console.WriteLine("-----------------------");
            //        Console.WriteLine("发送");
            //        Console.WriteLine("01 04 00 00 00 08 F1 CC");
            //        Console.WriteLine("接收");
            //        string res = new UtilsSerial().getPortData("/dev/ttyS3", 9600, "01 04 00 00 00 08 F1 CC");
            //        if (res.Length != 62)
            //        {
            //            Console.WriteLine("///////////////////////////");
            //        }
            //        Console.WriteLine(res);
            //        Console.WriteLine(System.DateTime.Now);
            //        Console.WriteLine("");
            //        // Thread.Sleep(100);
            //    }
            //});
            //Task.Run(() =>
            //{
            //    while (true)
            //    {
            //        Console.WriteLine("-----------------------");
            //        Console.WriteLine("发送");
            //        Console.WriteLine("01 06 00 00 00 01 48 0A");
            //        Console.WriteLine("接收");
            //        Console.WriteLine(new UtilsSerial().getPortData("/dev/ttyS3", 9600, "01 06 00 00 00 01 48 0A"));
            //        //Console.WriteLine("SSS");
            //         Thread.Sleep(100);
            //    }
            //});
            //Task.Run(() =>
            //{
            //    while (true)
            //    {
            //        Console.WriteLine("------s-----------------");
            //        Console.WriteLine("发送");
            //        Console.WriteLine("01 04 00 00 00 08 F1 CC");
            //        Console.WriteLine("接收");
            //        string res = new UtilsSerial().getPortData("/dev/ttyS3", 4800, "01 04 00 00 00 08 F1 CC");
            //        if (res.Length != 62)
            //        {

            //            Console.WriteLine("///////////////////////////");
            //        }
            //        Console.WriteLine(res);
            //        Console.WriteLine("------e-----------------");
            //        Console.WriteLine("");
            //        //Thread.Sleep(100);
            //    }
            //});
            Console.ReadLine();
        }
        public static void Run()
        {
            string device = "/dev/ttyS3";
            if (Setup.WiringPiPiSetup() < 0)
            {
                Console.WriteLine("Wringpi setup failed.");
                return;
            }

            int fd = Serial.SerialOpen(device, 4800);
            if (fd < 0)
            {
                Console.WriteLine("Serial ppen failed.");
                return;
            }


            //异步执行串口读取  
            Task readTask = Task.Run(() => SerialRead(fd));
            //发送
            while (true)
            {
                Console.Write("TX:");
                string send = Console.ReadLine();
                if (string.IsNullOrEmpty(send))
                {
                    continue;
                }
                if (send == "exit")
                {
                    break;
                }
                Serial.SerialPuts(fd, send);
            }
            Serial.SerialClose(fd);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey(false);
        }

        public static void SerialRead(int fd)
        {
            while (true)
            {
                StringBuilder sb = null;
                while (Serial.SerialDataAvail(fd) > 0)
                {
                    int read = Serial.SerialGetchar(fd);
                    if (read < 0)
                    {
                        Serial.SerialFlush(fd);
                        break;
                    }
                    else
                    {
                        //Remove \r or \n
                        if (read == 10 || read == 13)
                        {
                            continue;
                        }
                        else
                        {
                            if (sb == null)
                            {
                                sb = new StringBuilder();
                            }
                            sb.Append((char)read);
                        }
                    }
                }
                if (sb != null)
                {
                    string readStr = sb.ToString();
                    if (!string.IsNullOrWhiteSpace(readStr))
                    {
                        Console.WriteLine($"RX:{sb.ToString()}");
                    }
                }
            }
        }
    }
}
