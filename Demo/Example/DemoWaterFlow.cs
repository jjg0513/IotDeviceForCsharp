﻿using Iot.Device.Gpio.Drivers;
using IotDeviceToolHepler.DeviceForOrangePi;
using IotDeviceToolHepler.Utils;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.Example
{
    public class DemoWaterFlow
    {
        /// <summary>
        /// 准备，比较耗费资源
        /// </summary>
        public void run()
        {
            int total = 0;
            while (true)
            {
              
                int count = new DeviceExampleWaterFlow().getPulseCount_YFS201(10, 1);
                Console.WriteLine("脉冲数：" + count + " " + System.DateTime.Now.ToString());
                Console.WriteLine("-----------------");

                total = total + count;
                Console.WriteLine(total );
            }
        }
        /// <summary>
        /// wiringOP的回调方法，准确，推荐
        /// </summary>
        public void run3()
        {
            int pin = 10;
            DeviceExampleWaterFlow deviceExampleWaterFlow = new DeviceExampleWaterFlow();
            deviceExampleWaterFlow.getPulseCount_YFS201_V3(pin);
            while (true)
            {
                Thread.Sleep(1000);
                Console.WriteLine("脉冲数2：" + deviceExampleWaterFlow.pluseCount + " " + System.DateTime.Now.ToString());
                Console.WriteLine("-----------------");
            }
        }

        /// <summary>
        /// 微软方法，不太准确
        /// </summary>
        public void run2()
        {
            int pin = 78;
            DeviceExampleWaterFlow deviceExampleWaterFlow = new DeviceExampleWaterFlow();
            deviceExampleWaterFlow.getPulseCount_YFS201_V2(pin);          
            while (true)
            {            
                Thread.Sleep(1000);
                Console.WriteLine("脉冲数2：" + deviceExampleWaterFlow.pluseCount + " " + System.DateTime.Now.ToString());
                Console.WriteLine("-----------------");
            }
        }

     

      

        
    }
}
