﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;
using static System.Net.WebRequestMethods;
using IotDeviceToolHepler.Device;

namespace Demo.Example
{
    public class DemoByIOPort
    {

        /// <summary>
        /// RS485工业级光强度传感器B-RS-L30 光照度传感器，外形小巧，工作稳定，采用标准的 Modbus-RTU 通讯协议，适用于各种工业环境使用。
        /// </summary>
        public static void Run()
        {
            Sensor_LightBRSL30.test();
        }
    }
}

