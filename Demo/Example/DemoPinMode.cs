﻿using Iot.Device.Gpio.Drivers;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.Example
{
    public class DemoPinMode
    {
        public void run()
        {
            int count = 0;
            Setup.WiringPiPiSetup();
            while (true)
            {
                Setup.WiringPiPiSetup();
                GPIO.PinMode(2, WiringPi.Output);//设置引脚4为输出模式
                GPIO.DigitalWrite(2, WiringPi.High);//4引脚高电平
                Thread.Sleep(50);
                GPIO.DigitalWrite(2, WiringPi.Low);//4引脚低电平
                                                   // Thread.Sleep(50);
                Thread.Sleep(50);
                count = count+1;
                if(count>=9)
                {
                    break;
                }
            }


        }
    }
}
