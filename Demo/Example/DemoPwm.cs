﻿using Iot.Device.Gpio.Drivers;
using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.Example
{
    public class DemoPwm
    {
        public void run()
        {
            Setup.WiringPiPiSetup();
            GPIO.PinMode(7, WiringPi.Output);//设置引脚4为输出模式
            GPIO.PinMode(5, WiringPi.Output);//设置引脚4为输出模式
            //while (true)
            //{
                DateTime dt = DateTime.Now.AddSeconds(1);
                int count = 0;
                while (dt > DateTime.Now)
                {
                    GPIO.DigitalWrite(5, WiringPi.High);//4引脚高电平
                    GPIO.DigitalWrite(7, WiringPi.High);//4引脚高电平
                    //Thread.Sleep(1);
                    GPIO.DigitalWrite(5, WiringPi.Low);//4引脚低电平
                    GPIO.DigitalWrite(7, WiringPi.Low);//4引脚低电平
                    Thread.Sleep(10);
                    count = count + 1;
                }
                Console.WriteLine(count);
               // Thread.Sleep(1000);
            //}
        }
    }
}
