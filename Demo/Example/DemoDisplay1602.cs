﻿
using IotDeviceToolHepler.Device;

using IotDeviceToolHepler.WiringOPSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.Example
{
    public class DemoDisplay1602
    {
        public static void Run()
        {
            new DeviceDisplay_Lcd1602().Test();
        }
    }
}
